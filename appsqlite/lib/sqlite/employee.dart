
class Employee{
  int id;
  String name;
  String apellido;
  int edad;  

Employee (this.id, this.name, this.apellido, this.edad);

Map<String, dynamic> toMap(){
  var map = <String, dynamic>{
    'id': id,
    'name': name,
    'apellido':apellido,
    'edad':edad,

  };

  return map;
}
 Employee.fromMap(Map<String, dynamic> map){
   id = map['id'];
   name = map['name'];
   apellido =map['apellido'];
   edad = map['edad'];
 }
}
