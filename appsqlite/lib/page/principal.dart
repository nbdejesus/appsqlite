import 'package:appsqlite/sqlite/employee.dart';
import 'package:flutter/material.dart';
import 'dart:async';
import 'package:appsqlite/sqlite/db_helper.dart';
 
class DBPrincipalPage extends StatefulWidget {
  final String title;
 
  DBPrincipalPage({Key key, this.title}) : super(key: key);
 
  @override
  State<StatefulWidget> createState() {
    return _DBPrincipalPageState();
  }
}
 
class _DBPrincipalPageState extends State<DBPrincipalPage> {
  //
  Future<List<Employee>> employees;
  TextEditingController controllerName = TextEditingController();
  TextEditingController controllerApellido = TextEditingController();
  TextEditingController controllerEdad = TextEditingController();

  String name;
  String apellido;
  int edad;
  int curUserId;
 
  final formKey = new GlobalKey<FormState>();
  var dbHelper;
  bool isUpdating;
 
  @override
  void initState() {
    super.initState();
    dbHelper = DBHelper();
    isUpdating = false;
    refreshList();
  }
 
  refreshList() {
    setState(() {
      employees = dbHelper.getEmployees();
    });
  }
 
  clearName() {
    controllerName.text = '';
    controllerApellido.text = '';
    controllerEdad.text = '';
  }
 
  validate() {
    if (formKey.currentState.validate()) {
      formKey.currentState.save();
      if (isUpdating) {
        Employee e = Employee(curUserId, name, apellido, edad);
        dbHelper.update(e);
        setState(() {
          isUpdating = false;
        });
      } else {
        Employee e = Employee(null, name, apellido, edad);
        dbHelper.save(e);
      }
      clearName();
      refreshList();
    }
  }
 
  form() {
    return Form(
      key: formKey,
      child: Padding(
        padding: EdgeInsets.all(15.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          mainAxisSize: MainAxisSize.min,
          verticalDirection: VerticalDirection.down,
          children: <Widget>[
            TextFormField(
              controller: controllerName,
              keyboardType: TextInputType.text,
              decoration: InputDecoration(labelText: 'Name'),
              validator: (val) => val.length == 0 ? 'Enter Name' : null,
              onSaved: (val) => name = val,
            ),
             TextFormField(
              controller: controllerApellido,
              keyboardType: TextInputType.text,
              decoration: InputDecoration(labelText: 'Apellido'),
              validator: (val) => val.length == 0 ? 'Enter Apellido' : null,
              onSaved: (val) => apellido = val,
            ),
             TextFormField(
              controller: controllerEdad,
              keyboardType: TextInputType.number,
              decoration: InputDecoration(labelText: 'Edad'),
              validator: (val) => val.length == 0 ? 'Enter Edad' : null,
              onSaved: (val) => edad = val.hashCode,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                FlatButton(
                  onPressed: validate,
                  child: Text(isUpdating ? 'Actualizar' : 'Agregar'),
                ),
                FlatButton(
                  onPressed: () {
                    setState(() {
                      isUpdating = false;
                    });
                    clearName();
                  },
                  child: Text('Cancelar'),
                )
              ],
            ),
          ],
        ),
      ),
    );
  }
 
  SingleChildScrollView dataTable(List<Employee> employees) {
    return SingleChildScrollView(
      scrollDirection: Axis.vertical,
      child: DataTable(
        columns: [
          DataColumn(
            label: Text('NOMRE'),
          ),
             DataColumn(
            label: Text('APELLIDO'),
          ),
             DataColumn(
            label: Text('EDAD'),
          ),
          DataColumn(
            label: Text('ELIMINAR'),
          )
        ],
        rows: employees
            .map(
              (employee) => DataRow(cells: [
                    DataCell(
                      Text(employee.name),
                     
                      onTap: () {
                        setState(() {
                          isUpdating = true;
                          curUserId = employee.id;
                        });
                        controllerName.text = employee.name;
                   
                      },
                    ),
                       DataCell(
                      Text(employee.apellido),
                     
                      onTap: () {
                        setState(() {
                          isUpdating = true;
                          curUserId = employee.id;
                        });
                        controllerApellido.text = employee.apellido;
                   
                      },
                    ),
                       DataCell(
                     Text(employee.edad.toString()),
                     
                     
                      onTap: () {
                        setState(() {
                          isUpdating = true;
                          curUserId = employee.id;
                        });
                        controllerEdad.text = employee.edad.toString();
                   
                      },
                    ),
                    DataCell(IconButton(
                      icon: Icon(Icons.delete),
                      onPressed: () {
                        dbHelper.delete(employee.id);
                        refreshList();
                      },
                    )),
                  ]),
            )
            .toList(),
      ),
    );
  }
 
  list() {
    return Expanded(
      child: FutureBuilder(
        future: employees,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            return dataTable(snapshot.data);
          }
 
          if (null == snapshot.data || snapshot.data.length == 0) {
            return Text("No Data Found");
          }
 
          return CircularProgressIndicator();
        },
      ),
    );
  }
 
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text('Flutter SQLITE CRUD'),
      ),
      body: new Container(
        child: new Column(
          mainAxisAlignment: MainAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          verticalDirection: VerticalDirection.down,
          children: <Widget>[
            form(),
            list(),
          ],
        ),
      ),
    );
  }
}